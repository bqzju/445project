import random

#input: the text from the cloud
#output: useful variables tells from where go to where.  Or error1, which means wrong, should say again.
def info_text(text):
    #print("the length of text is:" , len(text))
    # it should be something like "one one go three five", strictly (1,1)->(3,5), otherwise wrong
    x_1 = ''
    y_1 = ''
    x_2 = ''
    y_2 = ''
    middle = ''
    word_list = [x_1,y_1,middle,x_2,y_2]
    word_cnt = 0 # 0 for word1, 4 for word5
    for i in range(0,len(text)):
        if text[i] == ' ': # it is a space between words
            word_cnt += 1
            continue
        if word_cnt>=5:
            break
        word_list[word_cnt] += text[i]
    if info_text_helper(word_list[0]) != -1: # a valid number from 1 to 8, [1,8]
        word_list[0] = info_text_helper(word_list[0])
    if info_text_helper(word_list[1]) != -1: # a valid number from 1 to 8, [1,8]
        word_list[1] = info_text_helper(word_list[1])
    if info_text_helper(word_list[3]) != -1: # a valid number from 1 to 8, [1,8]
        word_list[3] = info_text_helper(word_list[3])
    if info_text_helper(word_list[4]) != -1: # a valid number from 1 to 8, [1,8]
        word_list[4] = info_text_helper(word_list[4])
    number_valid = ( type(word_list[0])==type(0) ) and ( type(word_list[1])==type(0) )       \
                     and ( type(word_list[3])==type(0) ) and ( type(word_list[4])==type(0) )
    go_valid = (word_list[2] == "go")
    print("information is:", word_list)
    if number_valid == False:
        print("please tell a valid number!")
        return -1
    if go_valid == False:
        print("please say the word go")
        return -1
    print("command asks move from location", word_list[0],',',word_list[1], "to location", word_list[3],',' ,word_list[4])
    return word_list

def info_text_helper(word):
    #helper functions, returns the number of the word, or it is not a number in 1~8
    if word == "one":
        return 1
    if word == "two" or word == "too" or word == "to":
        return 2
    if word == "three":
        return 3
    if word == "four":
        return 4
    if word == "five":
        return 5
    if word == "six":
        return 6
    if word == "seven":
        return 7
    if word == "eight":
        return 8
    return -1 #not a number or the number is not in 1~8

#this function change number to letters, so that we can change (1,1) to a1, change (2,1) to b1
#note that there is no error check, the number should be strictly in 1~8
def num_to_letter(num):
    if num == 1:
        return "a"
    if num == 2:
        return "b"
    if num == 3:
        return "c"
    if num == 4:
        return "d"
    if num == 5:
        return "e"
    if num == 6:
        return "f"
    if num == 7:
        return "g"
    if num == 8:
        return "h"
    return "~"

#this function get the string used in chess move functinos, for example we get (1,2)->(3,4)
#then we will have the result "a2c4" returned. Below is a test code for it
# for x1 in range(1,9):
#     for y1 in range(1,9):
#         for x2 in range(1,9):
#             for y2 in range(1,9):
#                 print("change", x1,y1,x2,y2, "to",get_move_str(x1,y1,x2,y2))
def get_move_str(x1, y1, x2, y2):
    s1 = num_to_letter(x1)
    s2 = str(y1)
    s3 = num_to_letter(x2)
    s4 = str(y2)
    the_str = s1+s2+s3+s4
    return the_str





