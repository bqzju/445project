import requests
import json
import os
import base64
import chess
import f
import step
import make_audio
 
# the function use the cloud to transform a formed wav file to string text
def hi_cloud():
    #设置应用信息
    baidu_server = "https://openapi.baidu.com/oauth/2.0/token?"
    grant_type = "client_credentials"
    client_id = "rTKck5k8XTEA3PGCITTnRu3L" #填写API Key
    client_secret = "tWRmpyZADcpWQM8t1CgNbqekzjSGGxPe" #填写Secret Key
    
    #合成请求token的URL
    url = baidu_server+"grant_type="+grant_type+"&client_id="+client_id+"&client_secret="+client_secret
    #print("url:",url)
    
    #获取token
    res = requests.post(url)
    #print(res.text)
    token = json.loads(res.text)["access_token"]
    #print(token)
    #24.b891f76f5d48c0b9587f72e43b726817.2592000.1524124117.282335-10958516
    
    #设置格式
    RATE = "16000"
    FORMAT = "wav"
    CUID="wate_play"
    DEV_PID=1737  #here is number, not string.  1537 is for chinese, 1737 is for English
    
    #以字节格式读取文件之后进行编码
    with open(r'./audio/01.wav', "rb") as f:
        speech = base64.b64encode(f.read()).decode('utf8')
    size = os.path.getsize(r'./audio/01.wav')
    headers = { 'Content-Type' : 'application/json'}
    url = "https://vop.baidu.com/server_api"
    data={
    
            "format":FORMAT,
            "rate":RATE,
            "dev_pid":DEV_PID,
            "speech":speech,
            "cuid":CUID,
            "len":size,
            "channel":1,
            "token":token,
        }
    
    req = requests.post(url,json.dumps(data),headers)
    result = json.loads(req.text)
    print(result)
    text = result['result'][0]
    #right here we get a text, which is a string, with English words and spaces
    #note that numbers here like '1', are in the form of 'one'
    return text
#below are some simple codes to test hi_cloud()
#text = hi_cloud()
#print("the text is: " + text)

# board = chess.Board() #the origin game board, initialization
# print("the origin game board:")
# print(board)
# text = hi_cloud()
# print("the text we transform from the cloud is:", text)
# #text = "one three go three four"
# info = f.info_text(text)
# one_step_result = step.one_step_move(board, info[0],info[1],info[3],info[4])
# new_board = one_step_result[1]
# print("the new board after the step:")
# print(new_board)

#start the game
board = chess.Board()
print(board)
i = 1
while(i <= 1):  #game not end
    make_audio.record_one_file()  #store the recordings to a file called 01.wav
    text = hi_cloud()  #get the text
    info = f.info_text(text)
    if type(info) == int:
        i+=1
        continue
    step.one_step_move(board, info[0],info[1],info[3],info[4])
    print(board)
    i += 1






