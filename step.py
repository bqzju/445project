import chess
import f


# input: origin location(x1,y1), destination(x2,y2), the game board
# output: [-1,board] means fails, not a game logic move, the board will stay the same
#          [0,board] means success, a game logic move, and the board will change by this step.
def one_step_move(board, x1, y1, x2, y2):
    move_str = f.get_move_str(x1, y1, x2, y2)
    move = chess.Move.from_uci(move_str)
    if move not in list(board.legal_moves):
        print("you play a wrong step, it doesn't match the game logic. Please try again~")
        return [-1,board]
    board.push(move)
    print("one successful step!")
    return [0,board]
